#ifndef TIMER_MANAGER_HPP
#define TIMER_MANAGER_HPP

#include <set>
#include <memory>
#include <chrono>
#include <utility>
#include <functional>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/io_context.hpp>

class timer_manager
{
public:
    typedef int tid;

private:
    boost::asio::io_context &ioc;
    tid next_timerid;
    std::map<tid, boost::asio::steady_timer> timers;

public:
    timer_manager(boost::asio::io_context &ioc);

    template<typename Rep, typename Period>
    tid set_timeout(std::chrono::duration<Rep, Period> after, std::function<void()> action) {
        tid timerid = next_timerid++;
        timers.insert({timerid, boost::asio::steady_timer(ioc, after)});
        timers.at(timerid).async_wait([this, f = std::move(action), timerid](const boost::system::error_code &ec) {
            if (!ec) {
                f();
                timers.erase(timerid);
            }
        });
        return timerid;
    }

    int cancel(tid timerid);

    void cancel_all();
};

#endif // TIMER_MANAGER_HPP
